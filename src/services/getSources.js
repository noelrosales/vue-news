import axios from 'axios'
// import commonRequest from '@/utils/request'

export default async () => {
  const res = await axios.get('https://newsapi.org/v2/sources?apiKey=ff55591e19c74c018563bc1852ae1dde')
  // try {
  //   const res = await commonRequest('https://newsapi.org/v2/sources?apiKey=ff55591e19c74c018563bc1852ae1dde', 'GET', null, null)
  //   console.log('res', res)
  // } catch (error) {
  //   console.log(error)
  // }
  return res.data.sources
}
