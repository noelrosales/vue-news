import axios from './axios'

export default async ({ ...params }) => {
  const res = await axios.get('/top-headlines?apiKey=ff55591e19c74c018563bc1852ae1dde', {
    params: { ...params }
  })
  return res.data
}
