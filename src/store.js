import Vue from 'vue'
import Vuex from 'vuex'
import getNews from '@/services/getNews'
import getSources from '@/services/getSources'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    darkMode: true,
    sources: [],
    news: [],
    newsPage: 1,
    canFetchNews: false,
    searchParams: {},
    bookmarks: JSON.parse(localStorage.getItem('bookmarks')) || [],
    isLoading: false
  },
  getters: {
    getNews: state => {
      return state.news
    },
    getSources: state => {
      return state.sources
    },
    getBookmarks: state => {
      return state.bookmarks
    }
  },
  mutations: {
    setSources (state, sources) {
      state.sources = sources
    },
    setNews (state, news) {
      state.news = news
    },
    addNews (state, news) {
      state.news = [...state.news, ...news]
    },
    addBookmark (state, bookmark) {
      state.bookmarks = [...state.bookmarks, { ...bookmark, bookmarked: true }]
      localStorage.setItem('bookmarks', JSON.stringify(state.bookmarks))
    },
    removeBookmark (state, payload) {
      state.bookmarks = state.bookmarks.filter(bookmark => bookmark.title !== payload.title)
      localStorage.setItem('bookmarks', JSON.stringify(state.bookmarks))
    },
    setLoading (state, isLoading) {
      state.isLoading = isLoading
    },
    setTheme (state) {
      state.darkMode = !state.darkMode
    },
    setSearchParams (state, params) {
      state.searchParams = params
    },
    setNewsPage (state) {
      state.newsPage = 1
    },
    setNextPage (state) {
      state.newsPage += 1
    },
    setCanFetchNews (state, canFetch) {
      state.canFetchNews = canFetch
    }
  },
  actions: {
    async loadNews ({ commit, state }) {
      commit('setLoading', true)
      commit('setNewsPage')
      commit('setNews', [])
      try {
        const { articles, totalResults } = await getNews(state.searchParams)
        commit('setNews', articles)
        console.log(state.news.length)
        if(state.news.length < totalResults) {
          commit('setNextPage')
          commit('setCanFetchNews', true)
        } else {
          commit('setCanFetchNews', false)
        }
      } catch (error) {
        console.log(error)
      }
      commit('setLoading', false)
    },
    async loadMoreNews ({ commit, state }) {
      const nextParams = {...state.searchParams, page: state.newsPage}
      console.log('next', nextParams)
      commit('setLoading', true)
      try {
        const { articles, totalResults } = await getNews(nextParams)
        commit('addNews', articles)
        if(state.news.length < totalResults) {
          commit('setNextPage')
          commit('setCanFetchNews', true)
        } else {
          commit('setCanFetchNews', false)
        }
      } catch (error) {
        console.log(error)
      }
      commit('setLoading', false)
    },
    async loadSources ({ commit }) {
      commit('setLoading', true)
      try {
        const sources = await getSources()
        commit('setSources', sources)
      } catch (error) {
        console.log(error)
      }
      commit('setLoading', false)
    },
    addBookmark ({ commit }, bookmark) {
      commit('addBookmark', bookmark)
    },
    removeBookmark ({ commit }, bookmark) {
      commit('removeBookmark', bookmark)
    },
    toggleTheme ({ commit }) {
      commit('setTheme')
    }
  }
})
